Code of Conduct

1. When code or assets are added to the team repository, I accept that that is now the teams property and can be used even if I leave the team.

2. Sessions will be recorded.

3. Any ideas given during team collaboration sessions will be valued financially at zero, and will be owned on behalf of the team.

4. We will value different types of work such as code, design, documentation, interviews, research of equal value to the teams contribution.

5. Being present is not enough value, contribution of material is required.

6. Behaviour deemed less than expected by the team should be confronted at a team meeting at the earliest opportunity.

7. Communication at standups is going to be crucial.

8. Should a decision be required and the team are equally split may be with one in the middle, the team will accept the decision of the delivery staff.

9. If missing standup MUST send in actions for yesterday, today and blockers.

10. Everyone is responsible for ensuring they have ENOUGH to contribute and not taking on TOO much i.e.they must manage their own workloads.

11. The length of time you participate will not be reflected as a percentage of effort, once you leave the team, you are deemed to no longer be considered for part of any prizes.

12. Team members should work the backlog accordingly to ensure their time is productive and contributes to the team effort.  "Did not know what to do" is not a reasonable excuse.

13. If an individual has communicated and the scrum master a the time agreed to a leave of absence i.e. over 24 hours. Then any reward would be split as if the individual had not been required to take a leave of absence.

14. In an extended leave of absence, to be more than 1 week, the team will escalate to a member of the delivery staff, who will hold a meeting whereby all team members can discuss (a) whether the individual should be allocated an amount of any prize (b) give the team to vote on if the individual should be removed from the team.

15. 3 strikes system, where the team will vote on a problem with an individuals performance.  3 strikes earned will mean the team will remove that individual.  Strikes can be given in their absence but the delivery staff must be informed.

16. If the team feel a team member is abusing the good will of the others, this will be escalated to delivery staff, who will take the decision as to whether to remove that individual from the team.

17. If a team member after being removed, wants to come back. The team will vote and the majority vote will say whether the team will come back or not.

18. All team members make significant efforts at maintaining the Jira board.  Assigning tasks to individuals, moving them across the various columns to Done.  To adding any required new items into the backlog.

19. As a notional quantity of effort, each team member is expected to put in 20 hours (50% of the degree time) into the Call for Code team effort.

20. On a friday standup, the availability for the following week MUST be communicated and minuted.




