# University of Highlands and Islands - BSc Hons Applied Software Degree - Code of Conduct

This is the main document for you as a cohort to complete in Markdown.

## From Induction

    * Always be polite and courteous
    * Communicate little and often with your peers
    * Keep everyone updated with your progress
    * Be punctual to meetings, or give advance notice if going to missing or be late
    * Be open and willing to compromise
    * Remember to take coffee breaks
    * Treat everyone equally and consider each others backgrounds
    * Respect for each other
    * Remain professional
    * Avoid swearing and offensive terms
    * Be responsible and accountable for mistakes and delays

## In Week 8

+ Record common aspects and share with team - e.g. how to setup something

* Keep project communication transparent by using Slack only
* Member Availability
   - Scheduling meetings immediately before or after tutorials worked well
   - First thing in the morning was helpful as it gave a good start
   - Jira ticket was used, outlook calendars
   - Not doing meetings day by day as it was too hard to organise.
   - Forward planning meetings
   - Give more notice when unable to make a time when you thought you would.
   - Record meetings
   - Meeting should continue even if members were unable to make it.
   - If you miss a meeting, you should try to catch up as soon as you can.
 
* Daily meeting process
   - Agenda to cover items for each meeting - specific points
   - keep initial standup short, but that can move on into longer "coding" / "code review" session
   - Identify areas of weak understanding that should escalated to delivery team, could be working code not sure about or bugs that occured or existing challenges.

* Setting deadlines
    - setting story points, assignment (planning poker - Uncle Bob)
    - set number of tasks, not deadline
    - individual trello board for learning

* Task ownership & responsibility
    * To track tasks, use assignee
    * Checklists seem like a good idea for keeping track of individual completion but do not work.
    * Virtuous circle of thinking about how quickly tasks move from Backlog to Done and not getting stuck in a particular column.
    * Comments are key to be transparent with team on progress.
    * If task does not move quickly, split it up and remove the larger one.
    * (To try) Limit number of tasks of a particular type to increase variation amongst team members

* Breaking down the problem
    * Hard to do initially when knowledge is sparse.
    * Breaking down learning
    * Adding learning to Jira
    * Recording short videos of what you have learned for the others
    * Divide learning up - not everyone needs to learn everything
    * Checklists did not work for tracking learning.

* Jira Setup
    * Use of epics, maybe epic per person and a backlog epic to keep all tasks.
    * Use of the filters to see what an individual is doing.
    * Think about the level of detail on Epics, User stories and how it impacts discussion and creativity.
  
* Tracking commits
    * Use the reports in Jira to track progress, may be add them to daily standup.
  
* Getting help from team and escalating to delivery team
    * Finding resources that are at the wrong level,
    * Single file format and how that plays into a larger piece of work
    * Escalate challenges in understanding to team 

# Post Hackathon

* Carry out Design Thinking process to generate ideas
* Maybe have a agile coach - spotting members who were not conforming to the given process
* Burndown Chart Help: https://www.atlassian.com/agile/tutorials/burndown-charts 
* Note that teamwork can be more tiring with the interaction
* Motivation ebbs and flows, work at 70% to keep everyone "comfortable"
* At morning meeting, pick off fewer tasks for those
* Phone number for emergency communication
* Make sure you commit everything before you stop work
* Make sure all resources are available to the team
* Make sure Jira boards are updated before you stop work
* Limit the time for ideation/brainstorming (1 hour discussion max)
* Do not have "one person's idea", make sure the team arrive at consensus and "buy-in".
* Think about what you can bring "forward" and iterate to avoid crunches.
* Have escalation procedure for when team members get stuck.




