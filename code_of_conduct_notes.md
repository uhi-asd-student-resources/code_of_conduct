Notes from induction session

* Always be polite and courteous
* Communite little and often with your peers
* Keep everyone updated with your progress
* Be punctual to meetings, or give advance notice if going to missing or be late
* Be open and willing to compromise
* Remember to take coffee breaks
* Treat everyone equally and consider each others backgrounds
* Respect for each other
* Remain professional
* Avoid swearing and offensive terms
* Be responsible and accountable for mistakes and delays

Alexander Duthie wrote for Team 1/2:
Work Together
Treat others how you would like to be treated and work together. Do not discriminate your colleagues and treat everyone equally.
Respect Others
Respect your peers at all times.
Mind your Language
When in the workplace, remember to speak professionally at all times, even when speaking generally to people. Avoid swearing and offensive terms.
Be Polite and Courteous
Be polite to others, be patient, respect their boundaries and be wary of what you share/say to your peers. (i.e. media, images, videos, music)
Exercise Responsibility
Be responsible in the workplace and report any irresponsible behaviour to your superior.
Professionalism
Show professionalism in the workplace and towards your peers.

John Anderson from the team scenarios for Team 1

Put a plan in place with the team leader to ensure they don't fall out
10:22
Attending a teamworking workshop to improve their communication and teamwork
10:23
2. Create a plan at the start of each sprint, delegating the tasks to each person and when it's cemented everyone knows who is doing what and there is no fear of being left with all the work
10:24
Give the person with less time smaller tasks but a larger quantity of them, give the full time person the bigger tasks but not as many tasks
10:25
3. He should ask them questions that demonstrate to him that they understand the concepts e.g. this declares the doc type, but why do i do this? And making sure they can repeat it back to him
10:27
4. Planning meeting at the start of the sprint, discussing strengths and weaknesses and delegating tasks appropriately to maximise productivity. However people should still be pushed to learn new things i.e. delegate tasks such as 2 things someone is confident with and 2 they are less confident with. (At the end of it you should be comfortable at a decent level with everything)
10:29
5. She should try and build the confidence to express her feelings to the team, and then its the team's job to make things better for her. Or if there's one person she can confide in she should try to discuss it with them and then they can help make things better for her
10:31
6. Similar answer to 2
10:32
7. Don't let people volunteer to do tasks, instead delegate them in the planning meetings e.g. everyone has to take a certain number of points to ensure a reasonably even workload, taking into account the amount of time people have to work on tasks
10:33
Use Excel and visual guides to help with planning and seeing what needs to be done, or a shared document to see progress without daily meetings
10:35
8. Express their concerns and make sure Aziz feels the same way, their lack of experience would make them feel more concerned than Aziz might be. If Aziz feels the same way Emily and Darren could take on some of the jobs from Aziz to help complete the task in time
10:37
9. The team that attend should make notes during the meeting of what is discussed then send those on to Aziz, perhaps via a shared document or Excel
10:37
Or alternatively either Emily or Darren could have a quick 1-to-1 call with Aziz to discuss what was discussed in the meeting
10:39
10. Same as 9, also speak to Aziz first to see if he needs to attend the meetings. Maybe an afternoon meeting to get him up to speed with what happened in the morning meeting
10:39
Aziz could keep the team up to date with what he is doing before the meetings as well
10:40
11 Communicate her concerns over the lack of meetings with everyone, ask if people are needing the meeting time changed to a more suitable one
10:43
12 They should not stop the meeting, the meeting should be approached formally and be used as a checkpoint to make sure everyone knows where everyone is at and if they have accomplished what they aimed to the previous day. Gives structure and routine to each day as well. If daily is too frequent it could be reduced to every other day also. The meeting ensures everyone is on the same page and give people a chance to mention if they are struggling or haven't managed to achieve something. Keep track by making notes and bullet points. Keeping the meetings daily is important for helping people who might be struggling with their task. Good for motivation
10:44
Good for company culture, making sure everyone is comfortable around each other and the morale remains good
10:46
13. Everyone could create a chart of what times they are free, or maybe free, or not available so people know when they will get a response from someone
